import Image from "next/image";
import Link from "next/link";

export default function Footer() {
  return (
    <footer className="bg-color-900 text-color-100 text-sm">
      <div className="grid lg:grid-cols-5 gap-4 p-10 sm:grid-cols-2">
        <div className="lg:col-span-1 sm:col-span-2 m-auto">
          <Image src="/images/logo.webp" alt="Logo" width={60} height={60} />
        </div>
        <div className="col-span-2">
        Lorem Ipsum Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
        </div>
        <div className="text-center">
          <div><Link href="/">Home</Link></div>
          <div><a href="#">Create Article</a></div>
          <div><a href="#">Library</a></div>
          <div><a href="#">Collab India</a></div>
        </div>
        <div className="text-center">
          <div><a href="/signup">Signup</a></div>
          <div><a href="/login">Login</a></div>
          <div><Link href="/about">About Us</Link></div>
          <div><Link href="/about">Contact Us</Link></div>
        </div>
      </div>
      <hr className="mx-10"></hr>
      <div className="p-3 text-center">
        Copyright©2021 All Rights Reserved
      </div>
    </footer>
  );
}
