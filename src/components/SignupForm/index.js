"use client";

import { useState } from "react";

export default function SignupForm() {
  const [firstname, setFirstname] = useState("");
  const [lastname, setLastname] = useState("");
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState([]);
  const [success, setSuccess] = useState(false);

  const handleSubmit = async (e) => {
    e.preventDefault();

    const res = await fetch("api/signup", {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify({
        firstname,
        lastname,
        username,
        email,
        password,
      }),
    });

    const { message, success } = await res.json();
    setError(message);
    setSuccess(success);

    if (success) {
      setFirstname("");
      setLastname("");
      setUsername("");
      setEmail("");
      setPassword("");
    }
  };

  return (
    <div>
      <form
        onSubmit={handleSubmit}
        className="flex flex-col justify-center gap-3"
      >
        <div className="flex flex-col">
          <label htmlFor="firstname">First Name</label>
          <input
            onChange={(e) => setFirstname(e.target.value)}
            value={firstname}
            type="text"
            id="firstname"
            className=" border"
          />
        </div>
        <div className="flex flex-col">
          <label htmlFor="lastname">Last Name</label>
          <input
            onChange={(e) => setLastname(e.target.value)}
            value={lastname}
            type="text"
            id="lastname"
            className=" border"
          />
        </div>
        <div className="flex flex-col">
          <label htmlFor="username">Username</label>
          <input
            onChange={(e) => setUsername(e.target.value)}
            value={username}
            type="text"
            id="username"
            className=" border"
          />
        </div>
        <div className="flex flex-col">
          <label htmlFor="email">Email</label>
          <input
            onChange={(e) => setEmail(e.target.value)}
            value={email}
            type="email"
            id="email"
            className=" border"
          />
        </div>
        <div className="flex flex-col">
          <label htmlFor="password">Password</label>
          <input
            onChange={(e) => setPassword(e.target.value)}
            value={password}
            type="password"
            id="password"
            className="border"
            minLength={8}
            pattern="^(?=.*[A-Z])(?=.*[!@#$%^&*()_+|~-]).+$"
          />
        </div>
        <div>
          {error &&
            error.map((e, index) => (
              <small id="errorMessage" className="text-red-600" key={index}>
                {e}
                <br />
              </small>
            ))}
        </div>
        <div>
          <button
            type="submit"
            className="drop-shadow-md bg-color-900 text-white py-2 px-5"
          >
            Submit
          </button>
        </div>
      </form>
    </div>
  );
}
