"use client";
import Link from "next/link";
import Image from "next/image";
import { signIn, signOut, useSession } from "next-auth/react";

export default function Navbar() {
  const { status, data: session } = useSession();

  function toggleMenu() {
    const menu = document.querySelector(".mobile-menu");
    menu.classList.toggle("h-0");
    menu.classList.toggle("h-72");
  }
  return (
    <nav className="bg-color-900 text-color-100 text-sm">
      <div className="flex justify-between p-4 items-center">
        <div>
          <Image src="/images/logo.webp" alt="Logo" width={50} height={50} />
        </div>
        <div className="hidden md:flex justify-between gap-5">
          <div>
            <Link href="/">Home</Link>
          </div>
          <div>
            <Link href="/about">About Us</Link>
          </div>
          <div>
            <a href="#">Editors Search</a>
          </div>
          <div>
            <a href="#">Documentation</a>
          </div>
          <div>
            <a href="#">Library</a>
          </div>
          <div>
            <a href="#">Create Article</a>
          </div>
          <div>
            <a href="#">Noticeboard</a>
          </div>
        </div>
        {status === "authenticated" ? (
          <div className="hidden md:flex gap-5">
            <div>
              {session?.user?.name}
            </div>
            <div>
              <a href="#" onClick={() => signOut()}>
                Logout
              </a>
            </div>
          </div>
        ) : (
          <div className="hidden md:flex gap-5">
            <div>
              <a href="/login">Login</a>
            </div>
            <div>
              <a href="/signup">Signup</a>
            </div>
          </div>
        )}

        <div className="md:hidden flex">
          <button className="mobile-menu-button" onClick={toggleMenu}>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth="1.5"
              stroke="currentColor"
              className="w-6 h-6"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M3.75 9h16.5m-16.5 6.75h16.5"
              />
            </svg>
          </button>
        </div>
      </div>
      <div className="mobile-menu h-0 overflow-hidden px-4 transition-height duration-300 md:hidden">
        <div className="mt-2">
          <a href="#">Home</a>
        </div>
        <div className="mt-2">
          <a href="#">About Us</a>
        </div>
        <div className="mt-2">
          <a href="#">Editors Search</a>
        </div>
        <div className="mt-2">
          <a href="#">Documentation</a>
        </div>
        <div className="mt-2">
          <a href="#">Library</a>
        </div>
        <div className="mt-2">
          <a href="#">Create Article</a>
        </div>
        <div className="mt-2">
          <a href="#">Noticeboard</a>
        </div>

        {status === "authenticated" ? (
          <div className="mt-2">
            <a href="#" onClick={() => signOut()}>
              Logout
            </a>
          </div>
        ) : (
          <div>
            <div className="mt-2">
              <a href="/login">Login</a>
            </div>
            <div className="mt-2">
              <a href="/signup">Signup</a>
            </div>
          </div>
        )}
      </div>
    </nav>
  );
}
