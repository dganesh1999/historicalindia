import connectDB from "@/app/lib/mongodb";
import OAuth from "@/app/models/oauth";
import { NextResponse } from "next/server";

export async function POST(req) {
  const { name, email } = await req.json();

  await connectDB();
  await OAuth.create({ name, email });
  return NextResponse.json(
    {
      message: "User Registered",
    },
    {
      status: 201,
    }
  );
}
