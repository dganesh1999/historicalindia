import connectDB from "@/app/lib/mongodb";
import Signup from "@/app/models/signup";
import { hashPassword } from "@/app/utils/hashPasssword";
import mongoose from "mongoose";
import { NextResponse } from "next/server";

export async function POST(req) {
  const { firstname, lastname, username, email, password} = await req.json();
  const hpassword = await hashPassword(password);
  // console.log("------Password : ", hpassword);
  // password = hPassword;
  try {
    await connectDB();
    await Signup.create({ firstname, lastname, username, email, password});

    return NextResponse.json({
      message: ["Signup successful"],
      success: true,
    });
  } catch (error) {
    if (error instanceof mongoose.Error.ValidationError) {
      let errorList = [];
      for (let e in error.errors) {
        errorList.push(error.errors[e].message);
      }
      return NextResponse.json({ message: errorList });
    } else {
      console.log("Signup Failed by error: ", error);
      return NextResponse.json({ message: "Signup Failed" });
    }
  }
}
