import SignupForm from "@/components/SignupForm";

export default function Signup() {
  return (
    <div className="p-4 max-w-xl mx-auto">
      <div className="font-bold text-2xl">Signup</div>
      <SignupForm/>
    </div>
  );
}
