import mongoose, { Schema }  from "mongoose";

const signupSchema = new Schema({
    firstname: {
        type: String,
        required: [true, "First Name Required"],
        trim: true,
        minLength: [2, "Min 3 Characters"],
        maxLength: [20, "Max 20 Characters"],
    },

    lastname: {
        type: String,
        required: [true, "Last Name Required"],
        trim: true,
        minLength: [2, "Min 2 Characters"],
        maxLength: [20, "Max 20 Characters"],
    },

    username: {
        type: String,
        required: [true, "Username Required"],
        trim: true,
        match: [/^[a-zA-Z0-9]+$/, "Username should contain only letters and digits"],
        minLength: [4, "Min 4 Characters"],
        maxLength: [20, "Max 20 Characters"],
    },

    email: {
        type: String,
        trim: true,
        required: [true, "Email Required"],
    },

    password: {
        type: String,
        required: [true, "Password Required"],
        minLength: [8, "Min 8 characters"],
        maxLength: [20, "Max 20 characters"],
        match: [/^(?=.*[A-Z])(?=.*[!@#$%^&*()_+|~-]).+$/, "Password should have atleast one capital letter and one special character"],
    },

    date: {
        type: Date,
        default: Date.now,
    },
});

const Signup = mongoose.models.Signup || mongoose.model('User', signupSchema);

export default Signup;