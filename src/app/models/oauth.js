import mongoose, { Schema } from "mongoose";

const oauthUsers = new Schema(
  {
    name: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

const OAuth = mongoose.models.OAuth || mongoose.model('OAuthUser', oauthUsers);

export default OAuth;
