import Link from "next/link";

export default function About() {
  return (
    <div className="px-20 py-10 bg-color-100">
      <div className="px-4 py-6 my-10 rounded-md bg-white">
        <div className="font-bold text-2xl my-2 border-b-2 pb-4 border-color-gray">About Us</div>
        <div>
          Namaste. We at Historical India are a group of committed individuals,
          having an interest and fascination for Indian history and way of life.
          For several decades, the study of Indian history has been crippled by
          interventions that often sidestep the overwhelming reality of India
          being a living, breathing civilisation. We as a team are unflinching
          in our pride for Bharatavarsha and have created this platform for
          stimulating a discourse on Indian history, rooted in ideals emerging
          from this great nation of ours. 
          <br></br>
          <br></br>
          We function purely on a voluntary
          basis, and are welcoming in our fold individuals and groups wholly
          committed to the same ideal. This goal of creating a one-stop platform
          for learning about India’s glorious past cannot be achieved by any
          single individual or team, and that is precisely why you can consider
          Historical India to be a community devoted to this cause. We are
          reaching out to many history enthusiasts, ranging from school students
          to established scholars. If you even receive the slightest intuition
          of being one among them, think no further and join us on this journey
          that is sure to be rewarding for everyone on board. 
          <br></br>
          <br></br>
          In case you have
          any constructive feedback on our content, we highly encourage you to
          contact us. This will help immensely in achieving our goals, and also
          in the true Indian spirit, engaging with different ideas and
          perspectives...
        </div>
      </div>
      <div className="my-10 px-4 py-6 rounded-md bg-white">
        <div className="font-bold text-2xl my-2 border-b-2 pb-4 border-color-gray">Our Aim</div>
        <div>
          We are a community of history enthusiasts, united in our zeal to
          question the dominant narratives which govern the study of Indias
          past. Unapologetically proud of our civilizational heritage and wholly
          committed to upholding the highest professional standards of
          historical research, we at Historical India are wedded to the
          principles of knowledge democratisation and academic integrity. In
          working voluntarily to build this not-for-profit initiative, our only
          objective is to provide a credible knowledge platform for a curious
          yet confused Indian society to study, engage with and contribute to
          the story of its own origins and development. We want to act as
          catalysts in this perhaps small, but noble-intentioned experiment in
          national self-discovery. We make no self-flattering claims of
          intellectual superstardom or inborn scholarship. Mind you, we are and
          most humbly hope to remain so, mere students of Indias great
          historical tradition. We envision Historical India to serve as an open
          and inclusive forum for critically engaging with ideas and
          perspectives on all things India. In doing so, we actively encourage
          others sharing a passion for Indian history and culture to join our
          community and contribute effectively in bringing about an Indic
          Knowledge Renaissance.
        </div>
      </div>
      <div className="px-4 py-6 rounded-md bg-white">
        <div className="font-bold text-2xl my-2 border-b-2 pb-4 border-color-gray">Contact Us</div>
        <Link href="mailto: contact.historicalindia@gmail.com" className="hover:border-b-2 text-color-500">contact.historicalindia@gmail.com</Link>
      </div>
    </div>
  );
}
