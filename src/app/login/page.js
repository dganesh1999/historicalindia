// When we use event handlers from client side, need to specify the use client
"use client";

import { signIn, useSession } from "next-auth/react";
import Image from "next/image";
import googleLogo from "../../../public/images/google_logo.png";

export default function Login() {
  const { status } = useSession();
  if (status === "authenticated") {
    // redirect('/');
  } else {
    // alert("Auth");
  }
  return (
    <div className=" h-auto m-20 flex justify-center">
      <button
        onClick={() => signIn("google")}
        className="flex items-center gap-2 bg-blue-600 text-white p-2 bold"
      >
        <Image
          src={googleLogo}
          alt="Google Logo"
          className="p-1 bg-slate-50 w-8"
        />
        Sign in with Google
      </button>
    </div>
  );
}
